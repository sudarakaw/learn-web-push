const
  Html = require('html-webpack-plugin'),
  ServiceWorker = require('serviceworker-webpack-plugin'),
  { resolve } = require('path')

module.exports = (_, argv) => {
  const
    isDebug =  argv.d || argv.debug

  return {
    'mode': isDebug ? 'development' : 'production',
    'entry': [ resolve(__dirname, 'src/app.js') ],

    'output': {
      'path': resolve(__dirname, 'web/'),
      'filename': 'app.js'
    },

    'plugins': [
      new Html({
        'template': resolve(__dirname, 'src/index.ejs'),
        'filename': 'index.html',
        'minify': isDebug ? false : {
          'collapseBooleanAttributes': true,
          'collapseWhitespace': true,
          'decodeEntities': true,
          'html5': true,
          'minifyCSS': true,
          'minifyJS': true,
          'removeAttributeQuotes': true,
          'removeComments': true,
          'removeRedundantAttributes': true,
          'removeScriptTypeAttributes': true,
          'removeStyleLinkTypeAttributes': true,
          'useShortDoctype': true
        }
      }),

      new ServiceWorker({
        'entry': resolve(__dirname, 'src/sw.js'),
        'excludes': [
          '**/.*',
          '**/*.map',
          '*.html'
        ]
      })
    ]
  }
}
