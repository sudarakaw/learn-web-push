# Makefile: main build script
#
# Copyright 2018 Sudaraka Wijesinghe <sudaraka@sudaraka.org>
#

HOST ?=127.0.0.1
PORT ?=5000
VAPID_PUBLIC_KEY_FILE ?=public-key.txt
VAPID_PRIVATE_KEY_FILE ?=private-key.txt
WORKING_DIR ?=$(realpath .)
DIST_DIR ?=${WORKING_DIR}/web
SRC_DIR ?=${WORKING_DIR}/src
CGI_IMAGE ?=webpush-cgi
CGI_CONTAINER ?=${CGI_IMAGE}-container

VAPID_KEY_PEM=vapid-key.pem

all: build-server build-client

${DIST_DIR}/vendor/autoload.php: composer.*
	COMPOSER=../composer.json composer install --no-dev -d ${DIST_DIR}

${VAPID_KEY_PEM}:
	openssl ecparam -name prime256v1 -genkey -noout -out $@

${VAPID_PRIVATE_KEY_FILE}: ${VAPID_KEY_PEM}
	openssl ec -in $< -outform DER \
		| tail -c +8 \
		| head -c 32 \
		| base64 \
		| tr -d '\n\r= ' \
		| tr '+/' '-_' \
		> $@

${VAPID_PUBLIC_KEY_FILE}: ${VAPID_KEY_PEM}
	openssl ec -in $< -pubout -outform DER \
		| tail -c 65 \
		| base64 \
		| tr -d '\n\r= ' \
		| tr '+/' '-_' \
		> $@

keys: ${VAPID_PRIVATE_KEY_FILE} ${VAPID_PUBLIC_KEY_FILE}

${DIST_DIR}/Config.php: ${SRC_DIR}/Config.phps ${VAPID_PUBLIC_KEY_FILE} ${VAPID_PRIVATE_KEY_FILE}
	sed \
		-e "s/%VAPID_PUBLIC_KEY%/`cat ${VAPID_PUBLIC_KEY_FILE}`/" \
		-e "s/%VAPID_PRIVATE_KEY%/`cat ${VAPID_PRIVATE_KEY_FILE}`/" \
		$< > $@

run-http:
	-@pkill caddy

	caddy -host ${HOST} -port ${PORT}

run-cgi:
	-@docker kill ${CGI_CONTAINER} 2>/dev/null || true
	-@docker rm -f ${CGI_CONTAINER} 2>/dev/null || true

	sleep 1 && docker run \
		--rm \
		-it \
		--name=${CGI_CONTAINER} \
		-v ${WORKING_DIR}:${WORKING_DIR} \
		-v /tmp:/tmp \
		${CGI_IMAGE} \
			php-fpm \
			-y ${WORKING_DIR}/php-fpm.conf

build-server: ${DIST_DIR}/vendor/autoload.php ${DIST_DIR}/Config.php

build-client: ${VAPID_PUBLIC_KEY_FILE}
	npx webpack \
		--define VAPID_PUBLIC_KEY="\"`cat ${VAPID_PUBLIC_KEY_FILE}`\"" \
		$(filter-out $@,$(MAKECMDGOALS))

build-docker: Dockerfile
	-@docker rmi -f ${CGI_IMAGE}

	sleep 1 && docker build -t ${CGI_IMAGE} .

clean:
	$(RM) ${VAPID_KEY_PEM}
	$(RM) ${VAPID_PUBLIC_KEY_FILE}
	$(RM) ${VAPID_PRIVATE_KEY_FILE}
	$(RM) ${DIST_DIR}/*.html
	$(RM) ${DIST_DIR}/*.js
	$(RM) ${DIST_DIR}/Config.php
	$(RM) -r ${DIST_DIR}/vendor

%:
	@:

.PHONEY: all run-http keys clean run-cgi build-docker ${VAPID_KEY_PEM}
