<?php

class Config {
  // VAPID keys
  const VAPID_PUBLIC_KEY  = '%VAPID_PUBLIC_KEY%';
  const VAPID_PRIVATE_KEY  = '%VAPID_PRIVATE_KEY%';
}
