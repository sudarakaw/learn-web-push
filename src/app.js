document.addEventListener('DOMContentLoaded', () => {
  let
    isPushEnabled = false

  const
    buttonSubscription = document.getElementById('btnSubscription'),
    buttonNotification = document.getElementById('btnNotification'),

    updateState = state => {
      if('loading' === state) {
        buttonSubscription.disabled = true
        buttonSubscription.title = ''
        buttonSubscription.textContent = 'loading...'
      }
      else if('unsupported' === state) {
        buttonSubscription.disabled = true
        buttonSubscription.title = 'This browser does not support push notifications'
        buttonSubscription.textContent = 'unsupported!'
      }
      else if('init' === state) {
        buttonSubscription.disabled = false
        buttonSubscription.title = ''
        buttonSubscription.textContent = 'Subscribe'

        isPushEnabled = false
      }
      else if('push-on' === state) {
        buttonSubscription.disabled = false
        buttonSubscription.title = ''
        buttonSubscription.textContent = 'Unsubscribe'

        isPushEnabled = true
      }
      else if('push-off' === state) {
        updateState('init')
      }
      else {
        console.error('Unsupported state:', state)
      }
    },

    subscribe = () => {
      updateState('loading')

      navigator.serviceWorker.ready
        .then(reg => reg.pushManager.subscribe({
            'userVisibleOnly': true,
            'applicationServerKey': encodeKey(VAPID_PUBLIC_KEY)
          })
        )
        .then(subscriptionToServer('POST'))
        .then(sub => updateState('push-on'))
        .catch(e => {
          notificationDeniedOr(() => {
            console.error('Unable to subscribe to push notifications', e)

            updateState('push-off')
          })
        })
    },

    unsubscribe = () => {
      updateState('loading')

      navigator.serviceWorker.ready
        .then(reg => reg.pushManager.getSubscription())
        .then(sub => {
          if(!sub) {
            updateState('push-off')

            return
          }

          return subscriptionToServer('DELETE')(sub)
        })
        .then(sub => sub.unsubscribe())
        .then(() => updateState('push-off'))
        .catch(e => {
          console.error('Failed to unsubscribe', e)

          updateState('push-off')
        })
    },

    updateSubscription = () => {
      navigator.serviceWorker.ready
        .then(reg => reg.pushManager.getSubscription())
        .then(sub => {
          updateState('push-off')

          if(!sub) {
            return Promise.reject()
          }

          return subscriptionToServer('PUT')(sub)
        })
        .then(() => updateState('push-on'))
        .catch(e => {
          if(e) {
            console.error('Failed to update subscription', e)
          }
        })
    },

    subscriptionToServer = method => sub => {
      const
        key = sub.getKey('p256dh'),
        token = sub.getKey('auth'),
        contentEncoding = (PushManager.supportedContentEncodings || [ 'aesgcm' ])[0]

      return fetch(
        'subscription.php',
        {
          method,
          'body': JSON.stringify
            ( { 'endpoint': sub.endpoint
              , 'publicKey': key ? btoa(String.fromCharCode.apply(null, new Uint8Array(key))) : null
              , 'authToken': token ? btoa(String.fromCharCode.apply(null, new Uint8Array(token))) : null
              , contentEncoding
              }
            )
        }
      )
      .then(() => sub)
    }

    encodeKey = key => {
      const
        padding = '='.repeat((4 - key.length % 4) % 4),
        b64 = (key + padding)
          .replace(/\-/g, '+')
          .replace(/_/g, '/'),
        raw = atob(b64),
        output = new Uint8Array(raw.length)

      for(let i = 0; i < raw.length; i += 1) {
        output[i] = raw.charCodeAt(i)
      }

      return output
    },

    notificationDeniedOr = f => {
      if('denied' === Notification.permission) {
        console.warn('User denied notification usage.')
        updateState('unsupported')

        return true
      }
      else if('function' === typeof f) {
        f()
      }

      return false
    }

  if(!navigator.serviceWorker) {
    console.warn('No service worker support.')
    updateState('unsupported')

    return
  }

  if(!window.PushManager || !ServiceWorkerRegistration.prototype.showNotification) {
    console.warn('No push notification support.')
    updateState('unsupported')

    return
  }

  // Even if service worker & notifications are available, user MUST have given
  // permission to use them.
  if(notificationDeniedOr()) {
    return
  }

  navigator.serviceWorker.register('sw.js')
    .then(() => {
      updateSubscription()

      console.log('SW ready')
    })
    .catch(e => {
      console.error('Failed to register service worker\n', e)
      updateState('unsupported')
    })

  buttonSubscription.addEventListener('click', () => {
    if(isPushEnabled) {
      unsubscribe()
    }
    else {
      subscribe()
    }
  })

  buttonNotification.addEventListener('click', () => {
    navigator.serviceWorker.ready
      .then(reg => reg.pushManager.getSubscription())
      .then(sub => {
        if(!sub) {
          alert('Please subscribe to notifications')

          return
        }

        const
          contentEncoding = (PushManager.supportedContentEncodings || [ 'aesgcm' ])[0]

        return fetch(
          'notification.php',
          {
            'method': 'POST',
            // IMPORTANT: we MUST NOT sent subscription in a real application.
            // Subscription must already be in the database.
            'body': JSON.stringify
              ( Object.assign(sub.toJSON() , { contentEncoding })
              )
          }
        )
      })
  })

  updateState('init')
})
