<?php

$subscription = json_decode(file_get_contents('php://input'), true);

if(empty($subscription['endpoint'])) {
  echo 'Error: not a subscription';

  exit;
}

$method = $_SERVER['REQUEST_METHOD'];

if('POST' === $method) {
  // Store subscription record with `endpoint` as primary key.
}
elseif('PUT' === $method) {
  // Update `key` & `token` of existing subscription by `endpoint`.
}
elseif('DELETE' === $method) {
  // Delete stored subscription by `endpoint`.
}
else {
  echo 'Error: unsupported request';

  exit;
}
