<?php

require './vendor/autoload.php';

use Minishlink\WebPush\WebPush;
use Minishlink\WebPush\Subscription;

// IMPORTANT: we are ONLY getting subscription parameters from request for the
// demo purpose.
// In a real life application, subscription parameters MUST be obtained from the
// database.
$sub = Subscription::create(json_decode(file_get_contents('php://input'), true));

$auth =
  [ 'VAPID' =>
    [ 'subject' => 'http://127.0.0.1:5000/'
    , 'publicKey' => Config::VAPID_PUBLIC_KEY
    , 'privateKey' => Config::VAPID_PRIVATE_KEY
    ]
  ];

$webPush = new WebPush($auth);

$result = $webPush->sendNotification
  ( $sub
  , 'Yo!!!'
  );

foreach($webPush->flush() as $report) {
  $endpoint = $report->getRequest()->getUri()->__toString();

  if($report->isSuccess()) {
    echo "SUCCESS: " . $endpoint;
  }
  else {
    echo "FAIL: " . $endpoint . ", " . $report->getReason();
  }
}
